package com.example.delo_final_project.controller;

import com.example.delo_final_project.forms.SubunitForm;
import com.example.delo_final_project.models.Subunit;
import com.example.delo_final_project.models.User;
import com.example.delo_final_project.services.SubunitService;
import com.example.delo_final_project.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class SubunitController {
    @Autowired
    private SubunitService subunitService;
    @Autowired
    private UsersService usersService;

    @GetMapping("/subunits")
    public String getSubunits(@AuthenticationPrincipal(expression = "id") Integer id, Model model){
        List<Subunit> subunitList = subunitService.allSubunits();
        User user = usersService.getById(id);
        if(user.getRole().toString().equals("ADMIN")){
            model.addAttribute("ADMIN", user.getRole());
        }
        model.addAttribute("subunits", subunitList);
        return "subunits";
    }


    @PostMapping("/add_subunit")
    public String addSubunit(@Valid SubunitForm form, BindingResult result, RedirectAttributes forRedirectModel){
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Есть ошибки на форме!");
            return "redirect:/subunits";
        }
        subunitService.addSubunit(form);
        return "redirect:/subunits";
    }
}
