package com.example.delo_final_project.controller;

import com.example.delo_final_project.forms.DocumentForm;
import com.example.delo_final_project.models.DocFiles;
import com.example.delo_final_project.models.Document;
import com.example.delo_final_project.models.Worker;
import com.example.delo_final_project.services.DocFileService;
import com.example.delo_final_project.services.DocumentsService;
import com.example.delo_final_project.services.WorkersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
public class DocumentsController {

    @Autowired
    private DocumentsService documentsService;
    @Autowired
    private WorkersService workersService;
    @Autowired
    private DocFileService docFileService;

    @GetMapping("/")
    public String getDefaultPage(){
        return "redirect:/documents";
    }

    @GetMapping("/documents")
    public String getDocuments(Model model) {
        List<Document> documents = documentsService.allDocuments();
        List<Worker> workers = workersService.getAllWorkers();
        model.addAttribute("documents", documents).addAttribute("workers", workers);
        return "documents";
    }

    @PostMapping("/add_document")
    public String addDocuments(DocumentForm form) {
        documentsService.addDocument(form);
        return "redirect:/documents";
    }
    @GetMapping("/files/{doc-id}/upload")
    public String getFilesUploadPage(Model model, @PathVariable("doc-id") Integer docId){
        Document document = documentsService.getById(docId);
        List<DocFiles> docFiles = docFileService.getDocFiles(docId);
        model.addAttribute("document", document).addAttribute("doc_files", docFiles);
        return "files-upload";
    }
    @PostMapping("/files/upload")
    public void uploadFile(@RequestParam("description") String description, @RequestParam("file") MultipartFile multipartFile){

    }
}

