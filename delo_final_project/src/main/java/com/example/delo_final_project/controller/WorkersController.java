package com.example.delo_final_project.controller;

import com.example.delo_final_project.forms.WorkerForm;
import com.example.delo_final_project.models.Document;
import com.example.delo_final_project.models.Subunit;
import com.example.delo_final_project.models.User;
import com.example.delo_final_project.models.Worker;
import com.example.delo_final_project.repositories.UsersRepository;
import com.example.delo_final_project.services.DocumentsService;
import com.example.delo_final_project.services.SubunitService;
import com.example.delo_final_project.services.UsersService;
import com.example.delo_final_project.services.WorkersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class WorkersController {

    @Autowired
    private WorkersService workersService;
    @Autowired
    private SubunitService subunitService;
    @Autowired
    private DocumentsService documentsService;
    @Autowired
    private UsersService usersService;

    @GetMapping("/workers")
    public String getWorkers(@AuthenticationPrincipal(expression = "id") Integer id, Model model) {
        List<Subunit> subunits = subunitService.allSubunits();
        List<Worker> workers = workersService.getAllWorkers();
        User user = usersService.getById(id);
        if(user.getRole().toString().equals("ADMIN")){
            model.addAttribute("ADMIN", user.getRole());
        }
        model.addAttribute("subunits", subunits).addAttribute("workers", workers);
        return "workers";
    }

    @PostMapping("/add_worker")
    public String addWorker(WorkerForm form) {
        workersService.addWorker(form);
        return "redirect:/workers";
    }

    @GetMapping("/worker/{worker-id}")
    public String getDocumentByAuthor(Model model, @PathVariable("worker-id") Integer workerId) {
        Worker worker = workersService.getWorker(workerId);
        List<Document> documentsIsSigner = documentsService.getDocumentsBySigner(workerId);
        List<Document> documentsIsAuthor = documentsService.getDocumentsByAuthor(workerId);
        model.addAttribute("worker", worker);
        model.addAttribute("docs_signer", documentsIsSigner);
        model.addAttribute("docs_author", documentsIsAuthor);
        return "documents-by-worker";
    }

//    @PostMapping("/add_worker")
//    public String addWorker(@RequestParam("firstName") String firstName,
//                            @RequestParam("lastName") String lastName,
//                            @RequestParam("post") String post,
//                            @RequestParam("subunit_id") Integer subunit_id){
//        workersService.save(Worker.builder()
//                .firstName(firstName)
//                .lastName(lastName)
//                .post(post)
//                .subunit(subunitService.getById(subunit_id))
//                .build());
//        return "redirect:/add_workers";
//    }
}

