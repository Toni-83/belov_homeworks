package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.WorkerForm;
import com.example.delo_final_project.models.Document;
import com.example.delo_final_project.models.Worker;

import java.util.List;

public interface WorkersService {
    void addWorker(WorkerForm form);

    List<Worker> getAllWorkers();

    void deleteWorker(Integer workerId);

    Worker getWorker(Integer workerId);
}
