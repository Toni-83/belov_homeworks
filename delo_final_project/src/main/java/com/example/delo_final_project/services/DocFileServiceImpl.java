package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.DocFileForm;
import com.example.delo_final_project.models.DocFiles;
import com.example.delo_final_project.repositories.DocFilesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class DocFileServiceImpl implements DocFileService {
    private final DocFilesRepository docFilesRepository;
    @Override
    public List<DocFiles> getDocFiles(Integer id) {
        return docFilesRepository.getDocFilesByDocument_Id(id);
    }

    @Override
    public void addDocFile(DocFileForm form) {
        String extension = form.getMultipartFile().getOriginalFilename()
                .substring(form.getMultipartFile().getOriginalFilename().indexOf("."));
        docFilesRepository.save(DocFiles.builder()
                .originalName(form.getMultipartFile().getOriginalFilename())
                .storageName(UUID.randomUUID() + extension)
                .mimeType(form.getMultipartFile().getContentType())
                .size(form.getMultipartFile().getSize())
                .uploadDateTime(LocalDateTime.now())
                .build());
    }

    @Override
    public void deleteDocFie(Integer id) {
        docFilesRepository.deleteById(id);
    }
}
