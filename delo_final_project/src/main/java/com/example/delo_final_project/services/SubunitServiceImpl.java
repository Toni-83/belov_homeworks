package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.SubunitForm;
import com.example.delo_final_project.models.Subunit;
import com.example.delo_final_project.repositories.SubunitsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class SubunitServiceImpl implements SubunitService {
    @Autowired
    SubunitsRepository subunitsRepository;
    @Override
    public void addSubunit(SubunitForm form) {
        subunitsRepository.save(Subunit.builder().name(form.getName()).build());
    }

    @Override
    public List<Subunit> allSubunits() {

        return subunitsRepository.findAll();
    }

    @Override
    public void deleteSubunit(Integer subunitId) {

    }
    @Override
    public Subunit getSubunit(Integer subunit_id){
        return subunitsRepository.getById(subunit_id);
    }
}
