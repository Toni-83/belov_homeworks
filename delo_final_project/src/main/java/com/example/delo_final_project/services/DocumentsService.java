package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.DocumentForm;
import com.example.delo_final_project.models.Document;

import java.util.List;

public interface DocumentsService {

    List<Document> allDocuments();

    void addDocument(DocumentForm form);

    List<Document> getDocumentsByAuthor(Integer authorId);

    List<Document> getDocumentsBySigner(Integer signerId);

    Document getById(Integer id);
}
