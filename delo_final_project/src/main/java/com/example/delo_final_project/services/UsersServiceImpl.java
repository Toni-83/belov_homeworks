package com.example.delo_final_project.services;

import com.example.delo_final_project.models.User;
import com.example.delo_final_project.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    @Override
    public User getById(Integer id) {
        User user = usersRepository.getById(id);
        return user;
    }
}
