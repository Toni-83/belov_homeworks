package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.DocumentForm;
import com.example.delo_final_project.models.Document;
import com.example.delo_final_project.repositories.DocumentsRepository;
import com.example.delo_final_project.repositories.WorkersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DocumentsServiceImpl implements DocumentsService {
    private final DocumentsRepository documentsRepository;
    private final WorkersRepository workersRepository;

    @Override
    public List<Document> allDocuments() {

        return documentsRepository.findAll();
    }

    @Override
    public void addDocument(DocumentForm form) {
        documentsRepository.save(Document.builder()
                .name(form.getName())
                .documentType(form.getDocTipe())
                .author(workersRepository.getById(form.getAuthor_id()))
                .signer(workersRepository.getById(form.getSigner_id()))
                .build());

    }
    @Override
    public List<Document> getDocumentsByAuthor(Integer authorId){
        return documentsRepository.findAllByAuthor_Id(authorId);
    }
    @Override
    public List<Document> getDocumentsBySigner(Integer signerId){
        return documentsRepository.findAllBySigner_Id(signerId);
    }
    @Override
    public Document getById(Integer id){
        return documentsRepository.getById(id);
    }
}
