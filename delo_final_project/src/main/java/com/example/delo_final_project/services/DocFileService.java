package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.DocFileForm;
import com.example.delo_final_project.models.DocFiles;

import java.util.List;

public interface DocFileService {
    List<DocFiles> getDocFiles(Integer id);
    void addDocFile(DocFileForm form);
    void deleteDocFie(Integer id);
}
