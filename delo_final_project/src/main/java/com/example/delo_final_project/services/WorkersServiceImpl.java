package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.WorkerForm;
import com.example.delo_final_project.models.Document;
import com.example.delo_final_project.models.User;
import com.example.delo_final_project.models.Worker;
import com.example.delo_final_project.repositories.DocFilesRepository;
import com.example.delo_final_project.repositories.SubunitsRepository;
import com.example.delo_final_project.repositories.UsersRepository;
import com.example.delo_final_project.repositories.WorkersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class WorkersServiceImpl implements WorkersService {

    private final WorkersRepository workersRepository;
    private final UsersRepository usersRepository;
    private final SubunitsRepository subunitsRepository;
    private final PasswordEncoder passwordEncoder;
    private final DocFilesRepository docFilesRepository;

    @Override
    public void addWorker(WorkerForm form) {
        Integer user_id = usersRepository.save(User.builder()
                .userName(form.getUserName())
                        .hashPassword(passwordEncoder.encode(form.getPassword()))
                        .role(User.Role.valueOf(form.getRole()))
                .build()).getId();
        workersRepository.save(Worker.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .post(form.getPost())
                .subunit(subunitsRepository.getById(form.getSubunit_id()))
                        .user(usersRepository.getById(user_id))
                .build());
    }

    @Override
    public List<Worker> getAllWorkers() {
        User user = usersRepository.getById(2);
        List<Worker> workers = workersRepository.findAll();
        return workers;
    }

    @Override
    public void deleteWorker(Integer workerId) {

    }

    @Override
    public Worker getWorker(Integer workerId) {

        return workersRepository.getById(workerId);
    }

}
