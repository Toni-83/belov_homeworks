package com.example.delo_final_project.services;

import com.example.delo_final_project.forms.SubunitForm;
import com.example.delo_final_project.models.Subunit;

import java.util.List;

public interface SubunitService {
    void addSubunit(SubunitForm form);

    List<Subunit> allSubunits();

    void deleteSubunit(Integer subunitId);

    Subunit getSubunit(Integer subunit_id);
}
