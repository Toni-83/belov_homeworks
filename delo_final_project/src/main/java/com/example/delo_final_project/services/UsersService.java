package com.example.delo_final_project.services;

import com.example.delo_final_project.models.User;

public interface UsersService {
    User getById(Integer id);
}
