package com.example.delo_final_project.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "workers")
public class Worker {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "integer not null")
    private Integer id;
    private String firstName, lastName, post;


    //private String userName, role, subunit;

    @ManyToOne
    @JoinColumn(name = "subunit_id")
    private Subunit subunit;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "author")
    @ToString.Exclude
    private List<Document> documents;

    @OneToMany(mappedBy = "signer")
    @ToString.Exclude
    private List<Document> signedDocuments;
}
