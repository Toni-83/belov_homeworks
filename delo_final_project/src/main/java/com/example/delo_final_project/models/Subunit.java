package com.example.delo_final_project.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "subunits")
public class Subunit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String name;

//    @OneToMany(mappedBy = "subunit")
//    @ToString.Exclude
//    private List<Worker> workers;

}
