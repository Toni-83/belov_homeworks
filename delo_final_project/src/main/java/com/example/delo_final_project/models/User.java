package com.example.delo_final_project.models;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {
    public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "integer not null")
    private Integer id;

    @Column(unique = true)
    private String userName;

    private String hashPassword;

    //private Integer worker_id;

//    @OneToOne
//    @JoinColumn(name = "worker_id")
//    private Worker worker;

}
