package com.example.delo_final_project.models;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Documents")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "integer not null")
    private Integer id;
    private String name, documentType;
    //private Integer author_id, signer_id;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Worker author;

    @ManyToOne
    @JoinColumn(name = "signer_id")
    private Worker signer;

    @OneToMany(mappedBy = "document")
    @ToString.Exclude
    private List<DocFiles> docFiles;

}
