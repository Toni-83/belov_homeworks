package com.example.delo_final_project.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
@Data
public class DocFileForm {
    @NotEmpty
    private MultipartFile multipartFile;
    @NotEmpty
    @Length(max = 50)
    private Integer document_id;
}
