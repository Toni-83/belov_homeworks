package com.example.delo_final_project.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class SubunitForm {
    @NotEmpty
    @Length(max = 50)
    private String name;
}
