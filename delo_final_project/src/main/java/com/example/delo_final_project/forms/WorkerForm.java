package com.example.delo_final_project.forms;

import com.example.delo_final_project.models.Subunit;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class WorkerForm {
    @NotEmpty
    @Length(max = 10)
    private String firstName;
    @NotEmpty
    @Length(max = 10)
    private String lastName;
    @NotEmpty
    @Length(max = 30)
    private String post;
    @NotEmpty
    @Length(max = 10)
    private String userName;
    @NotEmpty
    @Length(max = 50)
    private String password;
    @NotEmpty
    private String role;
    @NotEmpty
    private Integer subunit_id;
}
