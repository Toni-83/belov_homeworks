package com.example.delo_final_project.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
@Data
public class DocumentForm {
    @NotEmpty
    @Length(max = 500)
    private String name;
    @NotEmpty
    @Length(max = 30)
    private String docTipe;

    private Integer author_id, signer_id;


}
