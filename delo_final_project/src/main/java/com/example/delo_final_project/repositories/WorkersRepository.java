package com.example.delo_final_project.repositories;


import com.example.delo_final_project.models.Worker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkersRepository extends JpaRepository<Worker, Integer> {
}
