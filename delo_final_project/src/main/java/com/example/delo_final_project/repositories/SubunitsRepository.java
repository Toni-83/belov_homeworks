package com.example.delo_final_project.repositories;

import com.example.delo_final_project.models.Subunit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubunitsRepository extends JpaRepository<Subunit, Integer> {
}
