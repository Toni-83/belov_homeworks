package com.example.delo_final_project.repositories;

import com.example.delo_final_project.models.DocFiles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DocFilesRepository extends JpaRepository<DocFiles, Integer> {
    List<DocFiles> getDocFilesByDocument_Id(Integer id);
}
