package com.example.delo_final_project.repositories;

import com.example.delo_final_project.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DocumentsRepository extends JpaRepository<Document, Integer> {
    List<Document> findAllByAuthor_Id(Integer id);
    List<Document> findAllBySigner_Id(Integer id);
}
